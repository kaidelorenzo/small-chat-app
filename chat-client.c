/*
 * chat-client.c
 */

#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <pthread.h>
#include <time.h>
#include <string.h>

#define BUF_SIZE 4096

void* receive_data_func (void *conn_fd);

int main(int argc, char *argv[])
{
  char *dest_hostname, *dest_port;
  struct addrinfo hints, *res;
  size_t conn_fd;
  char buf[BUF_SIZE];
  int n;
  int rc;


  dest_hostname = argv[1];
  dest_port     = argv[2];

  /* create a socket */
  conn_fd = socket(PF_INET, SOCK_STREAM, 0);

  /* client usually doesn't bind, which lets kernel pick a port number */

  /* but we do need to find the IP address of the server */
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  if((rc = getaddrinfo(dest_hostname, dest_port, &hints, &res)) != 0) {
      printf("getaddrinfo failed: %s\n", gai_strerror(rc));
      exit(1);
  }

  /* connect to the server */
  if(connect(conn_fd, res->ai_addr, res->ai_addrlen) < 0) {
      perror("connect");
      exit(2);
  }

  if(printf("Connected\n") < 0) {
    perror("printf");
    exit(EXIT_FAILURE);
  }

  pthread_t receive_data;
  if(pthread_create(&receive_data, NULL,receive_data_func, (void*) conn_fd) != 0){
    perror("pthread_create");
    exit(EXIT_FAILURE);
  }
  /* infinite loop of reading from terminal, sending the data, and printing
    * what we get back */
  while((n = read(0, buf, BUF_SIZE)) > 0) {
      if(send(conn_fd, buf, n, 0) == -1){
        perror("send");
        exit(EXIT_FAILURE);
      }
  }
  if(puts("Exiting.") == EOF){
    perror("puts");
    exit(EXIT_FAILURE);
  }

  if (close(conn_fd) == -1){
    perror("close");
    exit(EXIT_FAILURE);
  }
}

void* receive_data_func (void *conn_fd){
  int n;
  char buf[BUF_SIZE];
  char time_buf[9];
  time_t current_time;
  size_t thread_fd = (size_t) conn_fd;
  struct tm *local;

  while ((n = recv(thread_fd, buf, BUF_SIZE, 0))){
    current_time = time(NULL);
    local = localtime(&current_time);
    buf[n] = 0;

    if(local == NULL){
      perror("local time");
      exit(EXIT_FAILURE);
    }

    strftime(time_buf, 9, "%H:%M:%S:", local);


    if(printf("%s%s", time_buf, buf)< 0){
      perror("printf");
      exit(EXIT_FAILURE);
    }
    if(fflush(stdout) == -1){
      perror("fflush");
      exit(EXIT_FAILURE);
    }
  }
  if(puts("Connection closed by remote host.") == EOF){
    perror("puts");
    exit(EXIT_FAILURE);
  }
  exit(EXIT_SUCCESS);
  return NULL;
}
