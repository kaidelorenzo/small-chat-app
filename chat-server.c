/*
 * chat-server.c
 */

#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pthread.h>


#define BACKLOG 10
#define BUF_SIZE 4096
#define MAX_NICK 100

pthread_mutex_t mutex;

struct client_data{
  int conn_fd;
  char client_nick[MAX_NICK];
  size_t nick_length;
  struct sockaddr_in remote_sa;
  struct client_data *next;
  struct client_data *prev;
};

static struct client_data *first_client = NULL;



void *client_func(void *data);

void send_all(char *buf, int bytes_to_send);

int main(int argc, char *argv[])
{
  char *listen_port;
  int listen_fd, conn_fd;
  struct addrinfo hints, *res;
  int rc;
  struct sockaddr_in remote_sa;
  socklen_t addrlen;


  listen_port = argv[1];

  /* create a socket */
  if((listen_fd = socket(PF_INET, SOCK_STREAM, 0)) == -1){
    perror("socket");
    exit(EXIT_FAILURE);
  }

  /* bind it to a port */
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;
  if((rc = getaddrinfo(NULL, listen_port, &hints, &res)) != 0) {
    printf("getaddrinfo failed: %s\n", gai_strerror(rc));
    exit(1);
  }

  if (bind(listen_fd, res->ai_addr, res->ai_addrlen) == -1){
    perror("bind");
    exit(EXIT_FAILURE);
  }

  /* start listening */
  if (listen(listen_fd, BACKLOG) == -1) {
    perror("listen");
    exit(EXIT_FAILURE);
  }

  /* infinite loop of accepting new connections and handling them */
  while(1) {
    /* accept a new connection (will block until one appears) */
    addrlen = sizeof(remote_sa);
    if ((conn_fd = accept(listen_fd, (struct sockaddr *) &remote_sa, &addrlen)) == -1){
      perror("accept");
      exit(EXIT_FAILURE);
    }
    pthread_mutex_lock(&mutex);
    pthread_t client_thread;
    struct client_data *data = malloc(sizeof(struct client_data));
    if (!data){
      if(send(conn_fd, " Server overloaded\n", 19, 0) == -1){
        perror("send");
        exit(EXIT_FAILURE);
      }
      close(conn_fd);
    } else{
      data->conn_fd = conn_fd;
      data->remote_sa = remote_sa;
      strncpy(data->client_nick, " unknown", 9);
      data->nick_length = 7;

      if(!first_client){
        data->prev = NULL;
        data->next = NULL;
        first_client = data;
      }else{
        first_client->prev = data;
        data->next = first_client;
        data->prev = NULL;
        first_client = data;
      }
      pthread_mutex_unlock(&mutex);
      if (pthread_create(&client_thread, NULL, client_func, data) != 0){
        perror("pthread_create");
        exit(EXIT_FAILURE);
      }
    }
  }
}

void *
client_func(void *data){
  struct client_data *cd = data;
  /* announce our communication partner */
  uint16_t remote_port;
  char *remote_ip;
  char buf[BUF_SIZE];
  char data_to_send[BUF_SIZE + MAX_NICK];
  int bytes_received;
  remote_ip = inet_ntoa(cd->remote_sa.sin_addr);
  remote_port = ntohs(cd->remote_sa.sin_port);
  if(printf("new connection from %s:%d\n", remote_ip, remote_port) < 0) {
    perror("printf");
    exit(EXIT_FAILURE);
  }


  /* receive and echo data until the other end closes the connection */
  while((bytes_received = recv(cd->conn_fd, buf, BUF_SIZE, 0)) > 0) {
    buf[bytes_received] = 0;
    if(bytes_received > 5 && strncmp(buf, "/nick", 5) == 0){
      buf[bytes_received - 1] = 0;
      if(snprintf(data_to_send, BUF_SIZE + MAX_NICK, " User%s (%s:%d) is now known as%s.\n", cd->client_nick, remote_ip, remote_port, buf + 5) <0){
        perror("snprintf");
        exit(EXIT_FAILURE);
      }
      if(printf("%s", data_to_send + 1) < 0) {
        perror("printf");
        exit(EXIT_FAILURE);
      }
      send_all(data_to_send, strlen(data_to_send));
      cd->nick_length = bytes_received - 6;
      if (cd->nick_length > 100){
        cd->nick_length = 100;
      }
      strncpy(cd->client_nick, buf + 5, cd->nick_length);
      cd->client_nick[cd->nick_length] = 0;
    }else{
      if(snprintf(data_to_send, bytes_received + cd->nick_length + 4, "%s: %s", cd->client_nick, buf) < 0){
        perror("snprintf");
        exit(EXIT_FAILURE);
      }
      send_all(data_to_send, bytes_received + cd->nick_length + 4);
    }
  }


  if(printf("Lost connection from%s.\n", cd->client_nick) < 0){
    perror("printf");
    exit(EXIT_FAILURE);
  }

  if (close(cd->conn_fd) == -1){
    perror("close");
    exit(EXIT_FAILURE);
  }

  if(snprintf(data_to_send, BUF_SIZE + MAX_NICK, " User%s (%s:%d) has disconnected.\n", cd->client_nick, remote_ip, remote_port) < 0){
    perror("snprintf");
    exit(EXIT_FAILURE);
  }

  // send similar to above to all
  pthread_mutex_lock(&mutex);
  if(cd == first_client){
    if (first_client->next){
      first_client->next->prev = NULL;
      first_client = first_client->next;
    } else {
      first_client = NULL;
    }
  }else{
    if (cd->next){
      cd->prev->next = cd->next;
      cd->next->prev = cd->prev;
    } else {
      cd->prev->next = cd->next;
    }
  }
  pthread_mutex_unlock(&mutex);
  free(cd);

  send_all(data_to_send, strlen(data_to_send));

  return NULL;
}

void send_all (char *buf, int bytes_to_send){
  //loop over all file descriptors, call send
  if (first_client == NULL){
    // there are no current connections
    return;
  }
  pthread_mutex_lock(&mutex);
  struct client_data *current_client = first_client;
    //when i try to put an error check, it sends an error when we
    //close normally on one client using ctrl d
    if(send(current_client->conn_fd, buf, bytes_to_send, 0) == -1){
      perror("send");
      exit(EXIT_FAILURE);
    }

  while(current_client->next){
    current_client = current_client->next;
    if(send(current_client->conn_fd, buf, bytes_to_send, 0) == -1){
      perror("send");
      exit(EXIT_FAILURE);
    }
  }
  pthread_mutex_unlock(&mutex);
}
